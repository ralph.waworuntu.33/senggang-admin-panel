<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');

Route::prefix('admin/login')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'LoginAdminController@index')
        ->name('adminlogin');
    });


Route::prefix('admin')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'DashboardController@index')
        ->name('dashboard');
    });

Route::prefix('admin/product/category')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'CategoryController@index')
        ->name('category');
    });

Route::prefix('admin/product/category/sub')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'subCategoryController@index')
        ->name('subcategory');
    });

Route::prefix('admin/product')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'ProductListController@index')
        ->name('productlist');
    });

Route::prefix('admin/product/add')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'AddProductController@index')
        ->name('addproduct');
    });

Route::prefix('admin/sales/order')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'OrderController@index')
        ->name('order');
    });

Route::prefix('admin/sales/transaction')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'TransactionController@index')
        ->name('transaction');
    });

Route::prefix('admin/user')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'UserController@index')
        ->name('user');
    });


Route::prefix('admin/user/create')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'CreateUserController@index')
        ->name('createuser');
    });

Route::prefix('admin/vendors')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'VendorsController@index')
        ->name('vendors');
    });

Route::prefix('admin/vendors/create')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'CreateVendorsController@index')
        ->name('createvendors');
    });

Route::prefix('admin/reports')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'ReportsController@index')
        ->name('reports');
    });

Route::prefix('admin/settings/profile')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'ProfileController@index')
        ->name('profile');
    });

Route::prefix('admin/invoice')
    ->namespace('admin')
    ->group(function() {
        Route::get('/', 'InvoiceController@index')
        ->name('invoice');
    });

