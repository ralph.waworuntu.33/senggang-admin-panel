<!-- latest jquery-->
<script src="{{ url('backend/assets/js/jquery-3.3.1.min.js')}}"></script>

<!-- Bootstrap js-->
<script src="{{ url('backend/assets/js/popper.min.js')}}"></script>
<script src="{{ url('backend/assets/js/bootstrap.js')}}"></script>

<!-- feather icon js-->
<script src="{{ url('backend/assets/js/icons/feather-icon/feather.min.js')}}"></script>
<script src="{{ url('backend/assets/js/icons/feather-icon/feather-icon.js')}}"></script>

<!-- Sidebar jquery-->
<script src="{{ url('backend/assets/js/sidebar-menu.js')}}"></script>

<!--chartist js-->
<script src="{{ url('backend/assets/js/chart/chartist/chartist.js')}}"></script>



<!-- Chartjs -->
<script src="{{ url('backend/assets/js/chart/chartjs/chart.min.js') }}"></script>

<!-- lazyload js-->
<script src="{{ url('backend/assets/js/lazysizes.min.js')}}"></script>

<!--copycode js-->
<script src="{{ url('backend/assets/js/prism/prism.min.js')}}"></script>
<script src="{{ url('backend/assets/js/clipboard/clipboard.min.js')}}"></script>
<script src="{{ url('backend/assets/js/custom-card/custom-card.js')}}"></script>

<!--counter js-->
<script src="{{ url('backend/assets/js/counter/jquery.waypoints.min.js')}}"></script>
<script src="{{ url('backend/assets/js/counter/jquery.counterup.min.js')}}"></script>
<script src="{{ url('backend/assets/js/counter/counter-custom.js')}}"></script>

<!--Customizer admin-->
<script src="{{ url('backend/assets/js/admin-customizer.js')}}"></script>

<!--map js-->
<script src="{{ url('backend/assets/js/vector-map/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{ url('backend/assets/js/vector-map/map/jquery-jvectormap-world-mill-en.js')}}"></script>

<!--apex chart js-->
<script src="{{ url('backend/assets/js/chart/apex-chart/apex-chart.js')}}"></script>
<script src="{{ url('backend/assets/js/chart/apex-chart/stock-prices.js')}}"></script>

<!--chartjs js-->
<script src="{{ url('backend/assets/js/chart/flot-chart/excanvas.js')}}"></script>
<script src="{{ url('backend/assets/js/chart/flot-chart/jquery.flot.js')}}"></script>
<script src="{{ url('backend/assets/js/chart/flot-chart/jquery.flot.time.js')}}"></script>
<script src="{{ url('backend/assets/js/chart/flot-chart/jquery.flot.categories.js')}}"></script>
<script src="{{ url('backend/assets/js/chart/flot-chart/jquery.flot.stack.js')}}"></script>
<script src="{{ url('backend/assets/js/chart/flot-chart/jquery.flot.pie.js')}}"></script>

<!--dashboard custom js-->
<script src="{{ url('backend/assets/js/dashboard/default.js')}}"></script>

<!--right sidebar js-->
<script src="{{ url('backend/assets/js/chat-menu.js')}}"></script>

<!--height equal js-->
<script src="{{ url('backend/assets/js/equal-height.js')}}"></script>

<!-- lazyload js-->
<script src="{{ url('backend/assets/js/lazysizes.min.js')}}"></script>

<!--script admin-->
<script src="{{ url('backend/assets/js/admin-script.js')}}"></script>

<!-- Jsgrid js-->
<script src="{{ url('backend/assets/js/jsgrid/jsgrid.min.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/griddata-manage-product.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/jsgrid-manage-product.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/griddata-transactions.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/jsgrid-transactions.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/griddata-users.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/jsgrid-users.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/griddata-reports.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/jsgrid-reports.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/griddata-invoice.js') }}"></script>
<script src="{{ url('backend/assets/js/jsgrid/jsgrid-invoice.js') }}"></script>

<!-- touchspin js-->
<script src="{{ url('backend/assets/js/touchspin/vendors.min.js') }}"></script>
<script src="{{ url('backend/assets/js/touchspin/touchspin.js') }}"></script>
<script src="{{ url('backend/assets/js/touchspin/input-groups.min.js') }}"></script>

<!-- form validation js-->
<script src="{{ url('assets/js/dashboard/form-validation-custom.js') }}"></script>

<!--ckeditor js-->
<script src="{{ url('backend/assets/js/editor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('backend/assets/js/editor/ckeditor/styles.js') }}"></script>
<script src="{{ url('backend/assets/js/editor/ckeditor/adapters/jquery.js') }}"></script>
<script src="{{ url('backend/assets/js/editor/ckeditor/ckeditor.custom.js') }}"></script>


<!--Report chart-->
<script src="{{ url('backend/assets/js/admin-reports.js') }}"></script>

<!--dropzone js-->
<script src="{{ url('backend/assets/js/dropzone/dropzone.js') }}"></script>
<script src="{{ url('backend/assets/js/dropzone/dropzone-script.js') }}"></script>

<!-- Zoom js-->
<script src="{{ url('backend/assets/js/jquery.elevatezoom.js') }}"></script>
<script src="{{ url('backend/assets/js/zoom-scripts.js') }}"></script>

<!-- Datatable js-->
<script src="{{ url('backend/assets/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('backend/assets/js/datatables/custom-basic.js') }}"></script>

<!-- Google chart js-->
<script src="{{ url('backend/assets/js/chart/google/google-chart-loader.js') }}"></script>
