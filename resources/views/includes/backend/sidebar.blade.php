<!-- Page Sidebar Start-->
<div class="page-sidebar">
    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle lazyloaded blur-up" src="{{ url('backend/assets/images/dashboard/man.png')}}" alt="#">
            </div>
            <h6 class="mt-3 f-14">JOHN</h6>
            <p>Ux Designer</p>
        </div>
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{ route('dashboard') }}"><i data-feather="home"></i><span>Dashboard</span></a></li>
            <li><a class="sidebar-header" href="#"><i data-feather="box"></i> <span>Products</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                            <li><a href="{{ route('category') }}"><i class="fa fa-circle"></i>Category</a></li>
                            <li><a href="{{ route('subcategory') }}"><i class="fa fa-circle"></i>Sub Category</a></li>
                            <li><a href="{{ route('productlist') }}"><i class="fa fa-circle"></i>Product List</a></li>
                            <li><a href="{{ route('addproduct') }}"><i class="fa fa-circle"></i>Add Product</a></li>
                </ul>
            </li>
            <li><a class="sidebar-header" href=""><i data-feather="dollar-sign"></i><span>Sales</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{ route('order') }}"><i class="fa fa-circle"></i>Orders</a></li>
                    <li><a href="{{ route('transaction') }}"><i class="fa fa-circle"></i>Transactions</a></li>
                </ul>
            </li>
            <li><a class="sidebar-header" href=""><i data-feather="user-plus"></i><span>Users</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{ route('user') }}"><i class="fa fa-circle"></i>User List</a></li>
                    <li><a href="{{ route('createuser') }}"><i class="fa fa-circle"></i>Create User</a></li>
                </ul>
            </li>
            <li><a class="sidebar-header" href=""><i data-feather="users"></i><span>Vendors</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{ route('vendors') }}"><i class="fa fa-circle"></i>Vendor List</a></li>
                    <li><a href="{{ route('createvendors') }}"><i class="fa fa-circle"></i>Create Vendor</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header" href="{{ route('reports') }}"><i data-feather="bar-chart"></i><span>Reports</span></a></li>
            <li><a class="sidebar-header" href=""><i data-feather="settings" ></i><span>Settings</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{ route('profile') }}"><i class="fa fa-circle"></i>Profile</a></li>
                </ul>
            </li>
            <li><a class="sidebar-header" href="{{ route('invoice') }}"><i data-feather="archive"></i><span>Invoice</span></a>
            </li>
            <li><a class="sidebar-header" href="{{ route('adminlogin') }}"><i data-feather="log-in"></i><span>Logout</span></a>
            </li>
        </ul>
    </div>
</div>
<!-- Page Sidebar Ends-->

