<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

<!-- Font Awesome-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/font-awesome.css')}}">

<!-- Flag icon-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/flag-icon.css')}}">

<!-- Flag icon-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/chartist.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/jsgrid.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/themify.css') }}">

<!-- slick icon-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/slick-theme.css') }}">

<!-- ico-font-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/icofont.css')}}">

<!-- Prism css-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/prism.css')}}">

<!-- Chartist css -->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/chartist.css')}}">

<!-- vector map css -->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/vector-map.css')}}">

<!-- Bootstrap css-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/bootstrap.css')}}">

<!-- App css-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/admin.css')}}">

<!-- jsgrid css-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/jsgrid.css') }}">

<!-- Dropzone css-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/dropzone.css') }}">

<!-- Datatables css-->
<link rel="stylesheet" type="text/css" href="{{ url('backend/assets/css/datatables.css') }}">
