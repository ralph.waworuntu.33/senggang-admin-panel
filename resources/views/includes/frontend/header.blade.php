<!--header start-->
<header>
    <div class="mobile-fix-option"></div>

    <div class="layout-header2">
      <div class="container">
        <div class="col-md-12">
          <div class="main-menu-block">
            <div class="sm-nav-block">
              <span class="sm-nav-btn"><i class="fa fa-bars"></i></span>
              <ul class="nav-slide">
                <li>
                  <div class="nav-sm-back">
                    back <i class="fa fa-angle-right pl-2"></i>
                  </div>
                </li>
                <li><a href="#">western ware</a></li>
                <li><a href="#">TV, Appliances</a></li>
                <li><a href="#">Pets Products</a></li>
                <li><a href="#">Car, Motorbike</a></li>
                <li><a href="#">Industrial Products</a></li>
                <li><a href="#">Beauty, Health Products</a></li>
                <li><a href="#">Grocery Products </a></li>
                <li><a href="#">Sports</a></li>
                <li><a href="#">Bags, Luggage</a></li>
                <li><a href="#">Movies, Music </a></li>
                <li><a href="#">Video Games</a></li>
                <li><a href="#">Toys, Baby Products</a></li>
                <li class="mor-slide-open">
                  <ul>
                    <li><a href="#">Bags, Luggage</a></li>
                    <li><a href="#">Movies, Music </a></li>
                    <li><a href="#">Video Games</a></li>
                    <li><a href="#">Toys, Baby Products</a></li>
                  </ul>
                </li>
                <li>
                  <a class="mor-slide-click">
                    more category
                    <i class="fa fa-angle-down pro-down" ></i>
                    <i class="fa fa-angle-up pro-up" ></i>
                  </a>
                </li>
              </ul>
            </div>
            <div class="logo-block">
              <a href="index.html"><img src="../assets/images/layout-2/logo/logo.png" class="img-fluid  " alt="logo"></a>
            </div>
            <div class="input-block">
              <div class="input-box">
                <form class="big-deal-form">
                  <div class="input-group ">
                    <div class="input-group-prepend">
                      <span class="search"><i class="fa fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Cari Produk" >
                    <div class="input-group-prepend">
                      <select>
                        <option>All Category</option>
                        <option>indurstrial</option>
                        <option>sports</option>
                      </select>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="cart-block cart-hover-div " onclick="openCart()">
              <div class="cart ">
                <span class="cart-product">0</span>
                <ul>
                  <li class="mobile-cart  ">
                    <a href="#">
                      <i class="icon-shopping-cart "></i>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="cart-item">
                <h5>shopping</h5>
                <h5>cart</h5>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="category-header-2">
      <div class="custom-container">
        <div class="row">
          <div class="col">
            <div class="navbar-menu">
              <div class="category-left">
                <div class="nav-block">
                  <div class="nav-left" >
                    <nav class="navbar" data-toggle="collapse" data-target="#navbarToggleExternalContent">
                      <button class="navbar-toggler" type="button">
                        <span class="navbar-icon"><i class="fa fa-arrow-down"></i></span>
                      </button>
                      <h5 class="mb-0  text-white title-font">Shop by category</h5>
                    </nav>
                    <div class="collapse  nav-desk" id="navbarToggleExternalContent">
                      <ul class="nav-cat title-font">
                        <li> <img src="../assets/images/layout-1/nav-img/01.png" alt="category-product"> <a href="#">western ware</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/02.png" alt="category-product"> <a href="#">TV, Appliances</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/03.png" alt="category-product"> <a href="#">Pets Products</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/04.png" alt="category-product"> <a href="#">Car, Motorbike</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/05.png" alt="category-product"> <a href="#">Industrial Products</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/06.png" alt="category-product"> <a href="#">Beauty, Health Products</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/07.png" alt="category-product"> <a href="#">Grocery Products </a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/08.png" alt="category-product"> <a href="#">Sports</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/09.png" alt="category-product"> <a href="#">Bags, Luggage</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/10.png" alt="category-product"> <a href="#">Movies, Music </a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/11.png" alt="category-product"> <a href="#">Video Games</a></li>
                        <li> <img src="../assets/images/layout-1/nav-img/12.png" alt="category-product"> <a href="#">Toys, Baby Products</a></li>
                        <li>
                          <ul class="mor-slide-open">
                            <li> <img src="../assets/images/layout-1/nav-img/08.png" alt="category-product"> <a>Sports</a></li>
                            <li> <img src="../assets/images/layout-1/nav-img/09.png" alt="category-product"> <a>Bags, Luggage</a></li>
                            <li> <img src="../assets/images/layout-1/nav-img/10.png" alt="category-product"> <a>Movies, Music </a></li>
                            <li> <img src="../assets/images/layout-1/nav-img/11.png" alt="category-product"> <a>Video Games</a></li>
                            <li> <img src="../assets/images/layout-1/nav-img/12.png" alt="category-product"> <a>Toys, Baby Products</a></li>
                          </ul>
                        </li>
                        <li>
                          <a class="mor-slide-click">more category <i class="fa fa-angle-down pro-down"></i><i class="fa fa-angle-up pro-up"></i></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="menu-block">
                  <nav id="main-nav">
                    <div class="toggle-nav"><i class="fa fa-bars sidebar-bar"></i></div>
                    <ul id="main-menu" class="sm pixelstrap sm-horizontal">
                      <li>
                        <div class="mobile-back text-right">Back<i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                      </li>
                      <!--HOME-->
                      <li>
                        <a href="#" class="dark-menu-item">Home</a>
                      </li>
                      <!--HOME-END-->

                      <!--product-meu start-->
                      <li class="mega"><a href="#" class="dark-menu-item">Produk Terbaru</a>
                      </li>
                      <!--product-meu end-->

                      <!--blog-meu start-->
                      <li>
                        <a href="#" class="dark-menu-item">blog</a>
                      </li>
                      <!--blog-meu end-->
                    </ul>
                  </nav>
                </div>
                <div class="icon-block">
                  <ul>
                    <li class="onhover-dropdown" onclick="openAccount()">
                      <a href="#"><i class="icon-user"></i>
                      </a>
                    </li>
                    <li class="mobile-wishlist" onclick="openWishlist()">
                      <a ><i class="icon-heart"></i><div class="cart-item"><div>0 item<span>wishlist</span></div></div></a></li>
                    <li class="mobile-search"><a href="#"><i class="icon-search"></i></a>
                      <div class ="search-overlay">
                        <div>
                          <span class="close-mobile-search">×</span>
                          <div class="overlay-content">
                            <div class="container">
                              <div class="row">
                                <div class="col-xl-12">
                                  <form>
                                    <div class="form-group"><input type="text" class="form-control" id="exampleInputPassword1" placeholder="Search a Product"></div>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="mobile-setting mobile-setting-hover" onclick="openSetting()"><a href="#"><i class="icon-settings"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="category-right">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
