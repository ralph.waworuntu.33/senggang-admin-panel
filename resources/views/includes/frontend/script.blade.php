<!-- latest jquery-->
<script src="{{ url('frontend/assets/js/jquery-3.3.1.min.js')}}"></script>

<!-- slick js-->
<script src="{{ url('frontend/assets/js/slick.js')}}"></script>

<!-- popper js-->
<script src="{{ url('frontend/assets/js/popper.min.js')}}" ></script>

<!-- Timer js-->
<script src="{{ url('frontend/assets/js/menu.js')}}"></script>

<!-- Bootstrap js-->
<script src="{{ url('frontend/assets/js/bootstrap.js')}}"></script>

<!-- Bootstrap js-->
<script src="{{ url('frontend/assets/js/bootstrap-notify.min.js')}}"></script>

<!-- Theme js-->
<script src="{{ url('frontend/assets/js/script.js')}}"></script>
<script src="{{ url('frontend/assets/js/slider-animat.js')}}"></script>
<script src="{{ url('frontend/assets/js/timer.js')}}"></script>
<script src="{{ url('frontend/assets/js/modal.js')}}"></script>