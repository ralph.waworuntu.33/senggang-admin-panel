<!--footer start-->
<footer class="footer-2">
    <div class="container ">
        <div class="row">
            <div class="col-12">
                <div class="footer-main-contian">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 pr-lg-0">
                            <div class="footer-left">
                                <div class="footer-logo">
                                    <img src="../assets/images/layout-2/logo/logo.png" class="img-fluid" alt="logo">
                                </div>
                                <div class="footer-detail">
                                    <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock,</p>
                                    <ul class="paymant-bottom">
                                        <li><a href="#"><img src="../assets/images/layout-1/pay/1.png" class="img-fluid" alt="pay"></a></li>
                                        <li><a href="#"><img src="../assets/images/layout-1/pay/2.png" class="img-fluid" alt="pay"></a></li>
                                        <li><a href="#"><img src="../assets/images/layout-1/pay/3.png" class="img-fluid" alt="pay"></a></li>
                                        <li><a href="#"><img src="../assets/images/layout-1/pay/4.png" class="img-fluid" alt="pay"></a></li>
                                        <li><a href="#"><img src="../assets/images/layout-1/pay/5.png" class="img-fluid" alt="pay"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-12 p-l-md-0">
                            <div class="footer-right">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="subscribe-section">
                                            <div class="row">
                                                <div class="col-md-5 pr-lg-0">
                                                    <div class="subscribe-block">
                                                        <div class="subscrib-contant ">
                                                            <h4>Berlangganan</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 p-l-md-0">
                                                    <div class="subscribe-block">
                                                        <div class="subscrib-contant">
                                                            <div class="input-group" >
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" ><i class="fa fa-envelope-o" ></i></span>
                                                                </div>
                                                                <input type="text" class="form-control" placeholder="E-mail Anda" >
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text telly" ><i class="fa fa-telegram" ></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class=account-right>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="footer-box">
                                                        <div class="footer-title">
                                                            <h5>Akun Saya</h5>
                                                        </div>
                                                        <div class="footer-contant">
                                                            <ul>
                                                                <li><a href="#">Tentang Kami</a></li>
                                                                <li><a href="#">Hubungi Kami</a></li>
                                                                <li><a href="#">terms & conditions</a></li>
                                                                <li><a href="#">Pengembalian Barang</a></li>
                                                                <li><a href="#">Pengiriman</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="footer-box">
                                                        <div class="footer-title">
                                                            <h5>Jalan Pintas</h5>
                                                        </div>
                                                        <div class="footer-contant">
                                                            <ul>
                                                               
                                                                <li><a href="#"> Akun Saya</a></li>
                                                                <li><a href="#"> Lacak Pesanan</a></li>
                                                                
                                                                <li><a href="#">FAQ </a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="footer-box footer-contact-box">
                                                        <div class="footer-title">
                                                            <h5>Hubungi Kami</h5>
                                                        </div>
                                                        <div class="footer-contant">
                                                            <ul class="contact-list">
                                                                <li><i class="fa fa-map-marker"></i><span>Jl. Jenderal Sudirman No.37, Kuanino, Kec. Kota Raja, Kota Kupang, Nusa Tenggara Timur</span></li>
                                                                <li><i class="fa fa-phone"></i><span>Tlp : 123-456-7898</span></li>
                                                                <li><i class="fa fa-envelope-o"></i><span>E-mail: support@bigdeal.com</span></li>
                                                                <li><i class="fa fa-fax"></i><span>fax 123456</span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-link-block  bg-transparent">
        <div class="container">
            <div class="row">
                <div class="app-link-bloc-contain app-link-bloc-contain-1">
                    <div class="app-item-group">
                        <div class="app-item">
                            <a href="#"><img src="../assets/images/layout-1/app/1.png" class="img-fluid" alt="app-banner"></a>
                        </div>
                        <div class="app-item">
                            <a href="#"><img src="../assets/images/layout-1/app/2.png" class="img-fluid" alt="app-banner"></a>
                        </div>
                    </div>
                    <div class="app-item-group ">
                        <div class="sosiyal-block" >
                            <h6>Ikuti Kami</h6>
                            <ul class="sosiyal">
                                <li><a href="#"><i class="fa fa-facebook" ></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus" ></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" ></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" ></i></a></li>
                                <li><a href="#"><i class="fa fa-rss" ></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sub-footer-contain">
                        <p><span>2019 - 2020 </span>copy right by Senggang powered by Betablanja Dev</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--footer end-->