<!--Google font-->
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">

<!--icon css-->
<link rel="stylesheet" type="text/css" href="{{ url('frontend/assets/css/font-awesome.css')}}">
<link rel="stylesheet" type="text/css" href="{{ url('frontend/assets/css/themify.css')}}">

<!--Slick slider css-->
<link rel="stylesheet" type="text/css" href="{{ url('frontend/assets/css/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{ url('frontend/assets/css/slick-theme.css')}}">

<!--Animate css-->
<link rel="stylesheet" type="text/css" href="{{ url('frontend/assets/css/animate.css')}}">
<!-- Bootstrap css -->
<link rel="stylesheet" type="text/css" href="{{ url('frontend/assets/css/bootstrap.css')}}">

<!-- Theme css -->
<link rel="stylesheet" type="text/css" href="{{ url('frontend/assets/css/color2.css')}}" media="screen" id="color">