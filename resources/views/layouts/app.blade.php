<!DOCTYPE html>
<html lang="en">
<head>
  <title>SENGGANG</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="description" content="big-deal">
  <meta name="keywords" content="big-deal">
  <meta name="author" content="big-deal">
  <link rel="icon" href="{{ url('frontend/assets/images/favicon/favicon.ico')}}" type="image/x-icon">
  <link rel="shortcut icon" href="{{ url('frontend/assets/images/favicon/favicon.ico')}}" type="image/x-icon">

  @include('includes.frontend.style')

</head>
<body class="bg-light ">

@include('includes.frontend.loader')

@include('includes.frontend.header')

<!-- Content Start -->
@yield('content')
<!-- Content End -->

<!--footer start-->
@include('includes.frontend.footer')
<!--footer end-->

<!-- tap to top -->
<div class="tap-top">
  <div>
    <i class="fa fa-angle-double-up"></i>
  </div>
</div>
<!-- tap to top End -->

<!-- Add to cart bar -->
<div id="cart_side" class=" add_to_cart top">
  <a href="javascript:void(0)" class="overlay" onclick="closeCart()"></a>
  <div class="cart-inner">
    <div class="cart_top">
      <h3>my cart</h3>
      <div class="close-cart">
        <a href="javascript:void(0)" onclick="closeCart()">
          <i class="fa fa-times" aria-hidden="true"></i>
        </a>
      </div>
    </div>
    <div class="cart_media">
      <ul class="cart_product">
        <li>
          <div class="media">
            <a href="#">
              <img alt="" class="mr-3" src="{{ url('frontend/assets/images/layout-2/product/1.jpg')}}">
            </a>
            <div class="media-body">
              <a href="#">
                <h4>item name</h4>
              </a>
              <h4>
                <span>1 x $ 299.00</span>
              </h4>
            </div>
          </div>
          <div class="close-circle">
            <a href="#">
              <i class="ti-trash" aria-hidden="true"></i>
            </a>
          </div>
        </li>
        <li>
          <div class="media">
            <a href="#">
              <img alt="" class="mr-3" src="{{ url('frontend/assets/images/layout-2/product/a1.jpg')}}">
            </a>
            <div class="media-body">
              <a href="#">
                <h4>item name</h4>
              </a>
              <h4>
                <span>1 x $ 299.00</span>
              </h4>
            </div>
          </div>
          <div class="close-circle">
            <a href="#">
              <i class="ti-trash" aria-hidden="true"></i>
            </a>
          </div>
        </li>
        <li>
          <div class="media">
            <a href="#"><img alt="" class="mr-3" src="{{ url('frontend/assets/images/layout-2/product/1.jpg')}}"></a>
            <div class="media-body">
              <a href="#">
                <h4>item name</h4>
              </a>
              <h4><span>1 x $ 299.00</span></h4>
            </div>
          </div>
          <div class="close-circle">
            <a href="#">
              <i class="ti-trash" aria-hidden="true"></i>
            </a>
          </div>
        </li>
      </ul>
      <ul class="cart_total">
        <li>
          <div class="total">
            <h5>subtotal : <span>$299.00</span></h5>
          </div>
        </li>
        <li>
          <div class="buttons">
            <a href="cart.html" class="btn btn-normal btn-xs view-cart">view cart</a>
            <a href="#" class="btn btn-normal btn-xs checkout">checkout</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Add to cart bar end-->

<!--Newsletter modal popup start-->

<!--Newsletter Modal popup end-->

<!-- Quick-view modal popup start-->
<div class="modal fade bd-example-modal-lg theme-modal" id="quick-view" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content quick-view-modal">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="row">
          <div class="col-lg-6 col-xs-12">
            <div class="quick-view-img"><img src="{{ url('frontend/assets/images/layout-2/product/a1.jpg')}}" alt="" class="img-fluid "></div>
          </div>
          <div class="col-lg-6 rtl-text">
            <div class="product-right">
              <h2>Women Pink Shirt</h2>
              <h3>$32.96</h3>
              <ul class="color-variant">
                <li class="bg-light0"></li>
                <li class="bg-light1"></li>
                <li class="bg-light2"></li>
              </ul>
              <div class="border-product">
                <h6 class="product-title">product details</h6>
                <p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
              </div>
              <div class="product-description border-product">
                <div class="size-box">
                  <ul>
                    <li class="active"><a href="#">s</a></li>
                    <li><a href="#">m</a></li>
                    <li><a href="#">l</a></li>
                    <li><a href="#">xl</a></li>
                  </ul>
                </div>
                <h6 class="product-title">quantity</h6>
                <div class="qty-box">
                  <div class="input-group"><span class="input-group-prepend"><button type="button" class="btn quantity-left-minus" data-type="minus" data-field=""><i class="ti-angle-left"></i></button> </span>
                    <input type="text" name="quantity" class="form-control input-number" value="1"> <span class="input-group-prepend"><button type="button" class="btn quantity-right-plus" data-type="plus" data-field=""><i class="ti-angle-right"></i></button></span></div>
                </div>
              </div>
              <div class="product-buttons"><a href="#" class="btn btn-normal">add to cart</a> <a href="#" class="btn btn-normal">view detail</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Quick-view modal popup end-->




<!-- My account bar start-->
<div id="myAccount" class="add_to_cart right account-bar">
  <a href="javascript:void(0)" class="overlay" onclick="closeAccount()"></a>
  <div class="cart-inner">
    <div class="cart_top">
      <h3>my account</h3>
      <div class="close-cart">
        <a href="javascript:void(0)" onclick="closeAccount()">
          <i class="fa fa-times" aria-hidden="true"></i>
        </a>
      </div>
    </div>
    <form class="theme-form">
      <div class="form-group">
        <label for="email">Email</label>
        <input type="text" class="form-control" id="email" placeholder="Email" required="">
      </div>
      <div class="form-group">
        <label for="review">Password</label>
        <input type="password" class="form-control" id="review" placeholder="Enter your password" required="">
      </div>
      <div class="form-group">
        <a href="#" class="btn btn-rounded btn-block ">Login</a>
      </div>
      <div>
        <h5 class="forget-class"><a href="forget-pwd.html" class="d-block">forget password?</a></h5>
        <h6 class="forget-class"><a href="register.html" class="d-block">new to store? Signup now</a></h6>
      </div>
    </form>
  </div>
</div>
<!-- Add to account bar end-->

<!-- Add to wishlist bar -->
<div id="wishlist_side" class="add_to_cart right">
  <a href="javascript:void(0)" class="overlay" onclick="closeWishlist()"></a>
  <div class="cart-inner">
    <div class="cart_top">
      <h3>my wishlist</h3>
      <div class="close-cart">
        <a href="javascript:void(0)" onclick="closeWishlist()">
          <i class="fa fa-times" aria-hidden="true"></i>
        </a>
      </div>
    </div>
    <div class="cart_media">
      <ul class="cart_product">
        <li>
          <div class="media">
            <a href="#">
              <img alt="" class="mr-3" src="{{ url('frontend/assets/images/layout-1/media-banner/1.jpg')}}">
            </a>
            <div class="media-body">
              <a href="#">
                <h4>item name</h4>
              </a>
              <h4>
                <span>sm</span>
                <span>, blue</span>
              </h4>
              <h4>
                <span>$ 299.00</span>
              </h4>
            </div>
          </div>
          <div class="close-circle">
            <a href="#">
              <i class="ti-trash" aria-hidden="true"></i>
            </a>
          </div>
        </li>
        <li>
          <div class="media">
            <a href="#">
              <img alt="" class="mr-3" src="{{ url('frontend/assets/images/layout-1/media-banner/2.jpg')}}">
            </a>
            <div class="media-body">
              <a href="#">
                <h4>item name</h4>
              </a>
              <h4>
                <span>sm</span>
                <span>, blue</span>
              </h4>
              <h4>
                <span>$ 299.00</span>
              </h4>
            </div>
          </div>
          <div class="close-circle">
            <a href="#">
              <i class="ti-trash" aria-hidden="true"></i>
            </a>
          </div>
        </li>
        <li>
          <div class="media">
            <a href="#"><img alt="" class="mr-3" src="{{ url('frontend/assets/images/layout-1/media-banner/3.jpg')}}"></a>
            <div class="media-body">
              <a href="#"><h4>item name</h4></a>
              <h4>
                <span>sm</span>
                <span>, blue</span>
              </h4>
              <h4><span>$ 299.00</span></h4>
            </div>
          </div>
          <div class="close-circle">
            <a href="#">
              <i class="ti-trash" aria-hidden="true"></i>
            </a>
          </div>
        </li>
      </ul>
      <ul class="cart_total">
        <li>
          <div class="total">
            <h5>subtotal : <span>$299.00</span></h5>
          </div>
        </li>
        <li>
          <div class="buttons">
            <a href="wishlist.html" class="btn btn-normal btn-block  view-cart">view wislist</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Add to wishlist bar end-->

<!-- add to  setting bar  start-->
<div id="mySetting" class="add_to_cart right">
  <a href="javascript:void(0)" class="overlay" onclick="closeSetting()"></a>
  <div class="cart-inner">
    <div class="cart_top">
      <h3>my setting</h3>
      <div class="close-cart">
        <a href="javascript:void(0)" onclick="closeSetting()">
          <i class="fa fa-times" aria-hidden="true"></i>
        </a>
      </div>
    </div>
    <div class="setting-block">
      <div >
        <h5>language</h5>
        <ul>
          <li><a href="#">english</a></li>
          <li><a href="#">french</a></li>
        </ul>
        <h5>currency</h5>
        <ul>
          <li><a href="#">uro</a></li>
          <li><a href="#">rupees</a></li>
          <li><a href="#">pound</a></li>
          <li><a href="#">doller</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- facebook chat section end -->



@include('includes.frontend.script')

</body>
</html>
